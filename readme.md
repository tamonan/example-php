# Sobrecarga

Es el poder declarar mas de una funcion bajo el mismo nombre, pero con distintos parametros. Los parametros son distintos si tienen distinto tipo o cantidad.

1. sumar(int unValor, int otroValor) { return unValor + otroValor; }*

2. sumar(string unValor, string otroValor) { return parseInt(unValor) + parseInt(otroValor);}*

Entonces es definir dos funciones con el mismo nombre pero que se diferencia por el nombre.

Existe una relacion directa con el tipado.

### Tipado fuerte vs tipado debil:
Nosotros debemos definir el tipo de cada variable o objeto (tipado fuerte), esto se ve en JAVA int i; i = 3; si ahora queremos ponerle un string a i nos devolvera un error el compilador, ya que i solo admitira tipo int.
Los lenguajes debilmente tipados en cambio, uno no debe pre definir el tipo de la variable ( no rotular con ningun tipo a una variable), solo le asignamos el valor que deseamos que tenga, y pueda tener la flexibilidad de tener cualquier tipo luego.

En el lenguaje debilmente tipado, Uno al crear una function, no puede asignarle tipo a los parametros, entonces esto, no permitira crear metodos con mismo nombre y misma cantidad de parametro.

```
function getNombre(nombre) {
  console.log( nombre + “ es mi nombre”);
}
```

```
function getNombre(nombre, apellido) {
  console.log( nombre + “ “ + apellido + “ son mis nombres”);
}
```

Al intentar hacer uso de la primera, no se podra, ya que fue sobreescrita por la segunda.

Para simular la sobrecarga en javascript se puede hacer uso de 2 palabras reservadas:
arguments - un vector con los parametros
arguments.legth devuelve la cantidad de parametros
y podemos usar para mejorar ver el tipo de los parametros typeOf()

#### Ejemplos:
```
function getNombre() {
  if (arguments.length === 1) {
    console.log(arguments[0] + “es mi nombre”);
  } else {
    var nombres;
    for (var i = 0; i < arguments.length; i++) {
      nombres +=” ” + arguments[i];
    }
    console.log(“estos son mis nombres” + nombres);
}
```
```
function multiplicacion(valor1, valor2) {
  if (typeOf(valor1) = int) and (typeOf(valor2) = int) {
    total = valor1 * valor2
  } else if (typeOf(valor1) = float) and (typeOf(valor2) = float) {
    total = valor1 * valor2
  }
}
```

En php se puede simular parecido a javascript con las funcione funct_get_args() y funct_num_args(); y usar gettype() devuelve un string con el tipo de variable.


# Herencia

La herencia es la transmision de codigo entre una clase y otra. Para eso se debera tener una clase padre e hija. La clase hija hereda todo lo de la clase padre (metodos y propiedades).
Mecanismo que elementos especificos incorporan estructura y componente de elementos mas generales.
Donde es posible especializar o extender funcionalidad de una clase, derivando una nueva clase.

Hay transitividad de una clase a otra super clase, a varios niveles.

Puede ser herencia simple o multiple.

En PHP, la herencia es implementada usando la palabra clave extends.
```
class Animal {
  function hablar () {
    echo “yo no hablo, soy un animal”;
  }
}
class Perro extends Animal {
   function hablar() {
     echo “no hablo soy un perro”;
   }
}
```
La clase hija hereda las propieades y comportamientos de la clase padre.

Las restricciones de la herencia se dan:
Desde los modificadores, que segun cual tenga le da limitaciones a los metodos o propiedades.

*si son public sera accedido por cualquiera en el script.
si es private sera solo accedido dentro de la clase definida.
protected solo pertenece a la clase esa y sus descendientes
final -> no se puede sobreescribir en sus descendientes.
abstract -> no se lo puede usar, solo heredar.
por defecto todo es public.*

Otra restriccion es si es clase abstract no se la puede instanciar.


### Metodo de clase vs metodo de instancia:
los metodos de clase son los que no es necesario instanciar en un objeto, sino son directo sobre la clase.
En cambio los metodos de instancia se debera instanciar a un objeto de esta clase y luego hacer uso de ese metodo.
Ej:
Array es una clase en java que usamos y no la instanciamos, sino hacemos uso de metodos propios, al igual que Math class.


### Herencias multiples vs simples:
Java no permite herencias multiples.


### Clase abstracta

La clase abstracta se define con la palabra clave abstract class.
Si una clase tiene un metodo abstracto, esa clase debe ser abstracta.

Una clase que extiende de una clase abstracta debe definir los metodos que son abstractos.

### Interfaz

Se define con la palabra clave interface.
Lo que hace la interface es definir un contrato, estableciendo las condiciones bases.
Solo se define le cuerpo de las metodos, no el funcionamiento. Obliga a quienes implementen la interface a crear los metodos y darle el funcionamiento. Los metodos en la interface tienen el modificador publico.

### Clase concreta
Las clases que pueden usarse para instanciar objetos se denominan clases concretas.


# Sobre Escritura

Sobreescritura, es el redefinir un metodo que existe de un padre, en la clase hija, mismo nombre, puede hacer lo mismo o no, pero el punto es que se la vuelve a crear.

class padre {
  public function imprimir() {
    echo “padre imprime”;
  }
}

class hijo extends padre {
  public function imprimir() {
    echo “imprime el hijo”;
  }
}
ahora bien si no tenemos el metodo imprimir en el hijo, y una clase de tipo hijo realiza el mensaje imprimir, se ejecutara el del padre.
en php para llamar el metodo del padre se hace uso de la palabra clave parent::imprimir();
